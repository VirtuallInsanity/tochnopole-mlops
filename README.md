# Точнополе

Предсказание урожайности поля в зависимости от исторических параметров.

# Использованные метрики

Средняя абсолютная ошибка представляет собой среднее значение абсолютной разницы между фактическими и прогнозируемыми значениями в наборе данных. Он измеряет среднее значение остатков в наборе данных.

![mae](./images/mae.jpg)

Среднеквадратическая ошибка представляет собой среднее значение квадрата разницы между исходным и прогнозируемым значениями в наборе данных. Он измеряет дисперсию остатков.

![mse](./images/mse.jpg)

Коэффициент детерминации или R-квадрат представляет долю дисперсии зависимой переменной, которая объясняется моделью линейной регрессии. Это безмасштабная оценка, т.е. независимо от того, малы или велики значения, значение R-квадрата будет меньше единицы.

![r2](./images/r2.jpg)


Вывод предсказания урожайности:

![pred](./images/plot_pred.png)

# Датасет

Все данные были полученны из открытых источников (Росстат, Гисметео и т.д.)
