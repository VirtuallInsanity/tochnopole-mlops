import argparse

from src.predict import create_prediction


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--years_history", type=int, default=40, help='Amount of years to look back')
    parser.add_argument("--history_csv", type=str, required=True, default='data/rosselhoz-all-cereal-no_gross.csv',
                        help='Path to csv stats file')
    parser.add_argument("--trn_start", type=int, default=0, help='Starting year of training set')
    parser.add_argument("--trn_stop", type=int, default=2021, help='Ending year of training set')
    parser.add_argument("--frc_start", type=int, default=2022, help='Starting year of forecast')
    parser.add_argument("--frc_stop", type=int, default=2022, help='Ending year of forecast')

    parser.add_argument("--project_name", type=str, help='ClearML project name')
    parser.add_argument("--task_name", type=str, help='ClearML task name')

    return parser


if __name__ == '__main__':
    parser = create_parser()
    args = parser.parse_args()

    predicted_yield = create_prediction(args)
    print('Prediction: ', predicted_yield)
