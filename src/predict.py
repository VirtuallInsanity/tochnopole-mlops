import csv

from clearml import Task

import numpy as np

import pandas as pd

from sklearn.ensemble import GradientBoostingRegressor

from src.prep_data import create_Xy, split_Xy


def invboxcox(y, lmbda):
    """
    Data inverse of logarithm

    :param y: Input data
    :param lmbda: Coefficient
    :return: Inverted data
    """
    if lmbda == 0:
        return np.exp(y)
    else:
        return np.exp(np.log(lmbda * y + 1) / lmbda)


def make_forecast(length_frcst, data, model):
    """
    Utilize model to make a forecast on loaded data

    :param length_frcst: length of forecast in years
    :param data: input data
    :param model: forecast model
    :return: forecasted yield
    """
    forecasts = []
    input_features = data[0:1].to_numpy()

    num_cols = data.shape[1]

    counter = 1
    for i in range(length_frcst):
        forecast = model.predict(input_features)
        forecasts.append(forecast)
        counter += 1
        add_to_input = data[i + 1:i + 2].to_numpy()
        add_to_input = add_to_input[~np.isnan(add_to_input)].reshape(1, -1)
        input_features = np.hstack((np.array(forecasts)[::-1][:num_cols].reshape(1, -1), add_to_input))

    return np.array(forecasts).reshape(-1, 1)


def create_prediction(args):
    """
    Final prediction of future yield on csv file

    :param args: argparse arguments
    :return: yield prediction
    """
    task = Task.init(project_name=args.project_name, task_name=args.task_name)
    task.set_initial_iteration()
    my_data = pd.read_csv(args.history_csv)

    my_data['temp'] = np.log(abs(my_data['temp']))
    my_data['rainfall'] = np.log(my_data['rainfall'])
    my_data['cereal_area'] = np.log(my_data['cereal_area'])
    my_data['cereal_yield'] = np.log(my_data['cereal_yield'])

    if args.years_history > my_data.shape[0]:
        args.years_history = my_data.shape[0]
        print('Year out of range, using full history!')
    my_data = my_data[-args.years_history - 1:]

    my_data['Split'] = my_data['year'].astype(str)
    my_data.loc[(my_data['year'] >= args.trn_start) & (my_data['year'] <= args.trn_stop), 'Split'] = 'Train'

    my_data['cereal_area'] = my_data['cereal_area'].astype(float)

    data_frc = pd.DataFrame()
    data_frc['year'] = pd.DataFrame(range(args.frc_start, args.frc_stop + 1, 1))
    data_frc['temp'] = None
    data_frc['rainfall'] = None
    data_frc['cereal_area'] = None
    data_frc['cereal_yield'] = None
    data_frc['Split'] = 'Forecast'
    my_data = my_data.append(data_frc, ignore_index=True)

    col_period, col_temp, col_rainfall, col_area, col_target, col_split = \
        'year', 'temp', 'rainfall', 'cereal_area', 'cereal_yield', 'Split'
    shifts = np.arange(1, 13, 1)

    Xy = create_Xy(my_data, col_period, col_temp, col_rainfall, col_area, col_target, col_split, shifts)

    data = Xy
    features = Xy.columns.drop(['year', 'temp', 'rainfall', 'cereal_area', 'cereal_yield', 'Split'])

    X_train, y_train, X_test, y_test, X_forecast = split_Xy(data, features)

    model = GradientBoostingRegressor().fit(X_train, y_train)

    length_frcst = data_frc.shape[0]
    forecasts = make_forecast(length_frcst, X_forecast, model)
    forecasts_final = round(invboxcox(forecasts, lmbda=0)[0][0], 3)

    with open('./results/prediction.csv', 'w') as f:
        writer = csv.writer(f)
        writer.writerow([forecasts_final])

    return forecasts_final
