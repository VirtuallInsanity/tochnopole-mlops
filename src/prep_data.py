import pandas as pd


def split_Xy(data, features):
    """
    Splitting data to train, test & forecast sets

    :param data: Data to be splitted
    :param features: Useful features to keep
    :return: Splitted into different sets
    """
    X_train = pd.DataFrame()
    X_train[features] = data[features][data['Split'] == 'Train']
    y_train = pd.DataFrame()
    y_train['cereal_yield'] = data['cereal_yield'][data['Split'] == 'Train']

    X_test = pd.DataFrame()
    X_test[features] = data[features][data['Split'] == 'Test']
    y_test = pd.DataFrame()
    y_test['cereal_yield'] = data['cereal_yield'][data['Split'] == 'Test']

    X_forecast = pd.DataFrame()
    X_forecast[features] = data[features][data['Split'] == 'Forecast']

    return X_train, y_train, X_test, y_test, X_forecast


def create_Xy(data_ts, col_period, col_temp, col_rainfall, col_area, col_target, col_split, shifts):
    """
    Create data

    :param data_ts: Data time series
    :param col_period: Year column
    :param col_temp: Temperature column
    :param col_rainfall: Rainfall column
    :param col_area: Area column
    :param col_target: Target yield column
    :param col_split: Dataset split column
    :param shifts: Amount of years to shift window over
    :return: Dataset for prediction
    """
    Xy = pd.DataFrame()
    Xy['year'] = data_ts[col_period]
    Xy['temp'] = data_ts[col_temp]
    Xy['rainfall'] = data_ts[col_rainfall]
    Xy['cereal_area'] = data_ts[col_area]
    Xy['cereal_yield'] = data_ts[col_target]
    Xy['Split'] = data_ts[col_split]

    for shift in shifts:
        new_column = Xy['cereal_yield'].shift(shift).to_numpy()
        Xy['cereal_yield' + '_(t - ' + str(shift) + ')'] = new_column
        new_column = Xy['cereal_area'].shift(shift).to_numpy()
        Xy['cereal_area' + '_(t - ' + str(shift) + ')'] = new_column
        new_column = Xy['rainfall'].shift(shift).to_numpy()
        Xy['rainfall' + '_(t - ' + str(shift) + ')'] = new_column
        new_column = Xy['temp'].shift(shift).to_numpy()
        Xy['temp' + '_(t - ' + str(shift) + ')'] = new_column

    Xy = Xy[shift:]
    Xy = Xy[(Xy['Split'] == 'Train') | (Xy['Split'] == 'Test') | (Xy['Split'] == 'Forecast')]

    return Xy
