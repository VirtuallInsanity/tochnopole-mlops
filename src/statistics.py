import numpy as np

from sklearn.metrics import mean_squared_error, r2_score

import statsmodels.api as sm


def stats(true, predict):
    """
    Get stats regarding predicted and true value

    :param true: True value
    :param predict: Predicted value
    :return: Variation of stats
    """
    print('Average value: %u, RMSE: %u, %f%% (RMSE/AVG)' % (
        np.round(np.mean(true), 2),
        np.round(mean_squared_error(true, predict) ** 0.5, 2),
        100 * np.round(mean_squared_error(true, predict) ** 0.5, 2) / np.round(np.mean(true), 2)
    ))
    print('Coefficient of determination:', np.round(r2_score(true, predict), 4))

    errors = true - predict

    print('Остатки модели смещены на ' + str(np.round(errors.mean(), 2)))
    print('Смещение составляет ' + str(np.round(errors.mean() * 100 / predict.mean(), 2)) +
          '%' + ' от среднего значения прогнозируемой величины в', np.round(predict.mean(), 2))
    try:
        print('Критерий Дики-Фуллера: ' + 'p=' + str(np.round(sm.tsa.stattools.adfuller(errors)[1], 150)))
    except ValueError:
        print('Недостаточно примеров для: Критерий Дики-Фуллера')
